using System;
using System.Collections;
using Cathei.BakingSheet.Unity;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using ZBase.Collections.Pooled.Generic;

namespace DataManager
{
    public class DataService
    {
        private static DataService _instance;
        public static DataService Instance => _instance ??= new DataService();

        private SheetContainer container;

        private Dictionary<string, IDataModel> allData;

        public async void InitialData(Action onComplete = null)
        {
            allData = new Dictionary<string, IDataModel>();

            AsyncOperationHandle<SheetContainerScriptableObject> loadContainerHandle = Addressables.LoadAssetAsync<SheetContainerScriptableObject>(Data.SHEET_CONTAINE_SO_ADDRESS);
            await loadContainerHandle.Task;
            SheetContainerScriptableObject containerSO = loadContainerHandle.Result;
            ScriptableObjectSheetImporter importer = new(containerSO);

            container = new SheetContainer();
            await container.Bake(importer);

            container.Maps.ForEach(d => allData.TryAdd(d.Id, d));
            container.Fruits.ForEach(d => allData.TryAdd(d.Id, d));
            container.FruitContainers.ForEach(d => allData.TryAdd(d.Id, d));

            onComplete?.Invoke();
        }

        public T GetDataById<T>(string id) where T : class
        {
            if (allData != null && allData.ContainsKey(id) && allData[id] is T)
            {
                return allData[id] as T;
            }

            Debug.LogError("Cannot Find item ID " + id);
            return null;
        }

        public Map GetMapByIndex(int index)
        {
            return index < container.Maps.Count ? container.Maps[index] : null;
        }

        public IEnumerable GetFruitContainerID(Type type)
        {
            InitialData();

            if (container == null || container.FruitContainers == null) return null;
            List<ValueDropdownItem> items = new()
            {
                new ValueDropdownItem("None", "")
            };
            foreach (FruitContainer fruit in container.FruitContainers)
            {
                if (type == typeof(Tree) && fruit.Id.Contains('T'))
                {
                    items.Add(new ValueDropdownItem(fruit.Name, fruit.Id));
                }

                if (type == typeof(Booth) && fruit.Id.Contains('B'))
                {
                    items.Add(new ValueDropdownItem(fruit.Name, fruit.Id));
                }
            }

            return items;
        }

        public IEnumerable GetFruitID()
        {
            InitialData();

            if (container == null || container.Fruits == null) return null;
            List<ValueDropdownItem> items = new()
            {
                new ValueDropdownItem("None", "")
            };
            foreach (Fruit fruit in container.Fruits)
            {
                items.Add(new ValueDropdownItem(fruit.Name, fruit.Id));
            }

            return items;
        }
    }
}
