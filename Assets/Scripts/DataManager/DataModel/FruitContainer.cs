using Cathei.BakingSheet;

namespace DataManager
{
    public class FruitContainer : BaseModel
    {
        public string PrefabAddress { get; set; }
        public int BuyPrice { get; set; }
        public FruitSheet.Reference Fruit { get; set; }
    }

    public class FruitContainerSheet : Sheet<FruitContainer> { }
}
