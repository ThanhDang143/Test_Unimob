using System.Collections;
using System.Collections.Generic;
using Cathei.BakingSheet;
using Cathei.BakingSheet.Unity;


namespace DataManager
{
    public class Map : BaseModel
    {
        public string PrefabAddress { get; set; }
        public int RequireToPass { get; set; }
    }

    public class MapSheet : Sheet<Map> { }
}
