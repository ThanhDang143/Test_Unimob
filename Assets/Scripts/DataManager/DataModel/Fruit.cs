using System.Collections;
using System.Collections.Generic;
using Cathei.BakingSheet;
using Cathei.BakingSheet.Unity;


namespace DataManager
{
    public class Fruit : BaseModel
    {
        public string PrefabAddress { get; set; }
        public int SellPrice { get; set; }
    }

    public class FruitSheet : Sheet<Fruit> { }
}
