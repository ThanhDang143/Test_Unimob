public class Constants
{

}

public static class PrefabsAddress
{
    public const string Money = "Money";
    public const string Player = "Player";
    public const string Customer = "Customer";
    public const string Box = "Box";
}

public static class ScreenNames
{
    public const string HOME = "ScreenHome";
    public const string GAME = "ScreenGame";
    public const string END_GAME = "ScreenEndGame";
}

public static class PopupNames
{
    public const string SETTINGS = "PopupSettings";
    public const string PAUSE = "PopupPause";
    public const string WIN = "PopupWin";
}

public static class Data
{
    public const string SHEET_CONTAINE_SO_ADDRESS = "SheetContainer";
}

public static class NotificationKey
{
    public const string ON_RESOURCE_UPDATE = "OnResourceUpdate";
    public const string RESCAN_PATHFINDER = "RescanPathfinder";
    public const string CHECKOUT_O_DONE = "CheckoutDone";
}

public enum ResourceType
{
    Money,
}
