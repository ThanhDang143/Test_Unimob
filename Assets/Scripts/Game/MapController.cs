using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataManager;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(AstarPath))]
public class MapController : MonoBehaviour
{
    [Space(10)]
    [SerializeField] private AstarPath pathfinder;
    [SerializeField] private List<LockableObj> lockableObj;

    [Space(10)]
    [SerializeField] private Transform playerSpawnPoint;
    [SerializeField] private List<Transform> customerSpawnPoints;

    [Space(10)]
    [SerializeField] private float delaySpawnCustomer = 2f;

    private Map mapData;
    private Transform lastCustomerSpawnPoint;
    [HideInInspector] public CheckOutTable checkOutTable;
    private List<CustomerController> customers = new List<CustomerController>();
    private float delaySpawnCustomerCounter;

    [PropertySpace(SpaceBefore = 10)]
    [Button]
    public void Scan()
    {
        pathfinder = pathfinder != null ? pathfinder : GetComponent<AstarPath>();
        lockableObj = GetComponentsInChildren<LockableObj>().ToList();
        customerSpawnPoints = GetComponentsInChildren<Transform>().Where(t => t.name.Contains("CustomerSpawnPoint")).ToList();
        playerSpawnPoint = GetComponentsInChildren<Transform>().FirstOrDefault(t => t.name.Contains("PlayerSpawnPoint"));
    }

    private void OnDisable()
    {
        NotificationService.Instance.Unsubscribe(NotificationKey.RESCAN_PATHFINDER, ReScanPath);
    }

    private void Update()
    {
        SpawnMachine();
    }

    public void Init(Map _mapData)
    {
        mapData = _mapData;
        checkOutTable = lockableObj.FirstOrDefault(l => l is CheckOutTable) as CheckOutTable;
        customers ??= new List<CustomerController>();
        delaySpawnCustomerCounter = 0f;
        ScreenGame.Instance.SetRequiredMoney(mapData.RequireToPass);

        foreach (LockableObj obj in lockableObj)
        {
            obj.Init();
        }

        PlayerController player = Instantiate(GameManager.Instance.playerPrefab, transform).GetComponent<PlayerController>();
        player.transform.position = playerSpawnPoint.position;
        player.Init();

        NotificationService.Instance.Subscribe(NotificationKey.RESCAN_PATHFINDER, ReScanPath);
    }

    public int GetWinCondition()
    {
        return mapData.RequireToPass;
    }

    private void SpawnMachine()
    {
        delaySpawnCustomerCounter += Time.deltaTime;
        if (delaySpawnCustomerCounter >= delaySpawnCustomer)
        {
            SpawnCustomer();
        }
    }

    public List<Booth> GetActiveBooth()
    {
        return lockableObj.Where(b => b is Booth && b.IsUnlocked()).Cast<Booth>().ToList();
    }

    private void SpawnCustomer()
    {
        if (customers.Count < checkOutTable.CountStandingPoint())
        {
            delaySpawnCustomerCounter = 0f;
            CustomerController customer = Instantiate(GameManager.Instance.customerPrefab, transform).GetComponent<CustomerController>();
            customer.gameObject.name = $"Customer_{customers.Count + 1}";
            customers.Add(customer);
            customer.transform.position = GetSpawnPoint().position;
            customer.Init();
        }
        else
        {
            List<CustomerController> inactiveCustomers = customers.Where(c => c.IsState(CustomerState.Inactive)).ToList();
            if (inactiveCustomers.Count <= 0) return;

            delaySpawnCustomerCounter = 0f;
            CustomerController c = inactiveCustomers[Random.Range(0, inactiveCustomers.Count)];
            c.Init();
        }
    }

    public Transform GetSpawnPoint()
    {
        return lastCustomerSpawnPoint == null ? customerSpawnPoints[Random.Range(0, customerSpawnPoints.Count)] : customerSpawnPoints.Where(t => t != lastCustomerSpawnPoint).ToList()[Random.Range(0, customerSpawnPoints.Count - 1)];
    }

    private void ReScanPath()
    {
        pathfinder = pathfinder != null ? pathfinder : GetComponent<AstarPath>();
        pathfinder.Scan();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
