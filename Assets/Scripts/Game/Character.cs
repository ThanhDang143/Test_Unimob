using System.Collections;
using System.Collections.Generic;
using Animancer;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Space(10)]
    [SerializeField] protected CharacterController characterController;
    [SerializeField] protected AnimancerComponent animancer;
    [SerializeField] public Transform fruitContainer;
    [SerializeField] public List<FruitView> fruits;

    [Space(10)]
    [SerializeField] public float moveSpeed = 5f;
    [SerializeField] public int maxFruitCanCarry = 6;

    [Space(10)]
    [Header("Animation")]
    [SerializeField] protected ClipTransition idle;
    [SerializeField] protected ClipTransition idleWithItem;
    [SerializeField] protected ClipTransition run;
    [SerializeField] protected ClipTransition runWithItem;

    public virtual void Init()
    {
        characterController = characterController != null ? characterController : GetComponent<CharacterController>();
        animancer = animancer != null ? animancer : GetComponent<AnimancerComponent>();
        fruits ??= new List<FruitView>();
    }

    public bool CanCarry()
    {
        return fruits.Count < maxFruitCanCarry;
    }

    public bool IsCarrying()
    {
        return fruits.Count > 0;
    }

    public virtual void Update()
    {
        MovePlayer();
    }

    public virtual void MovePlayer()
    {

    }

    public virtual void PlayAnimation(float moveMagnitude)
    {
        if (fruits.Count <= 0)
        {
            animancer.Play(moveMagnitude > 0 ? run : idle);
        }
        else
        {
            animancer.Play(moveMagnitude > 0 ? runWithItem : idleWithItem);
        }
    }

    public Vector3 GetFruitPos(FruitView fruit)
    {
        return fruitContainer.position + (fruits.Count - 1) * fruit.GetSize().y * Vector3.up;
    }
}
