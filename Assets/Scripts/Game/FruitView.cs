using System;
using System.Collections;
using System.Linq;
using DataManager;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class FruitView : MonoBehaviour
{
    [Space(10)]
    [SerializeField, ValueDropdown("GetIDForInspecter")] protected string objID;

    private DataManager.Fruit data;
    private Vector3 size;

    private IEnumerable GetIDForInspecter()
    {
        return DataService.Instance.GetFruitID();
    }

    private void Awake()
    {
        if (!string.IsNullOrEmpty(objID)) data = DataManager.DataService.Instance.GetDataById<DataManager.Fruit>(objID);
        size = GetComponent<BoxCollider>().bounds.size;
    }

    public string GetID()
    {
        return data.Id;
    }

    public Vector3 GetSize()
    {
        return size;
    }

    public int GetSellPrice()
    {
        return data.SellPrice;
    }

    private void Update()
    {
        if (transform.localScale.x > 1.5f || transform.localScale.y > 1.5f || transform.localScale.z > 1.5f)
        {
            transform.localScale = Vector3.one;
        }
    }
}
