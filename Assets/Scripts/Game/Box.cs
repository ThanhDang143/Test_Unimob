using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class Box : MonoBehaviour
{
    [Space(10)]
    [SerializeField] private List<Transform> fruitPoss;
    [SerializeField] private List<Transform> boxCaps;

    public void Close()
    {
        boxCaps[0].DORotate(new Vector3(-126f, 0, 0), 0.5f).OnComplete(() =>
        {
            boxCaps[1].DORotate(new Vector3(0, 0, 129f), 0.5f);
        });
        boxCaps[2].DORotate(new Vector3(126f, 0, 0), 0.5f).OnComplete(() =>
        {
            boxCaps[3].DORotate(new Vector3(0, 0, -129f), 0.5f);
        });
    }

    public Transform GetEmptyFruitPos()
    {
        return fruitPoss.FirstOrDefault(f => f.childCount == 0);
    }
}
