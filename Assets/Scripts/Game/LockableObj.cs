using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cathei.BakingSheet.Unity;
using DataManager;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class LockableObj : MonoBehaviour
{
    [Space(10)]
    [SerializeField, ValueDropdown("GetID")] protected string objID;
    [SerializeField] protected TextMeshProUGUI txtUnlockPrice;

    [Space(10)]
    [SerializeField] public FruitContainerView fruitContainer;
    [SerializeField] protected Transform unlockGroup;
    [SerializeField] protected Transform lockGroup;
    [SerializeField] protected List<Character> characterNearby;
    [SerializeField] public List<FruitView> fruits;


    [Space(10)]
    [SerializeField] protected bool isUnlocked;
    [SerializeField] protected int resourcesNeededToUnlock;
    protected Coroutine unlockCoroutine;
    protected FruitContainer data;

    private Vector3 bottomScale;
    protected GameObject moneyPrefab;
    protected bool isUnlocking;

    private IEnumerable GetID()
    {
        return DataService.Instance.GetFruitContainerID(this.GetType());
    }

    public void LoadMoneyPrefab()
    {
        GameManager.Instance.LoadAssetAsync<GameObject>(PrefabsAddress.Money, (result) =>
        {
            moneyPrefab = result;
        });
    }

    public virtual void Init()
    {
        if (!string.IsNullOrEmpty(objID)) data = DataService.Instance.GetDataById<FruitContainer>(objID);
        isUnlocking = false;
        characterNearby ??= new List<Character>();
        LoadUnlockPrice();
        LoadMoneyPrefab();
        LoadUnlockedView();

        if (unlockGroup == null || lockGroup == null) return;
        ReloadLockStatus();
    }

    private void LoadUnlockPrice()
    {
        if (string.IsNullOrEmpty(objID) || isUnlocked) return;

        resourcesNeededToUnlock = data.BuyPrice;
        UpdateResourcesNeededToUnlock();
    }

    private void LoadUnlockedView()
    {
        // Case checkout Table have no ID
        if (fruitContainer != null)
        {
            bottomScale = fruitContainer.bottom.localScale;
            if (isUnlocked) NotificationService.Instance.Post(NotificationKey.RESCAN_PATHFINDER);
            return;
        }

        if (string.IsNullOrEmpty(objID)) return;
        GameManager.Instance.InstantiateAsync(data.PrefabAddress, unlockGroup, (result) =>
        {
            fruitContainer = result.GetComponent<FruitContainerView>();
            if (fruitContainer.bottom != null) bottomScale = fruitContainer.bottom.localScale;

            if (isUnlocked) NotificationService.Instance.Post(NotificationKey.RESCAN_PATHFINDER);
        });
    }

    public int CountStandingPoint()
    {
        return fruitContainer.standingPoints.Count;
    }

    public virtual Transform GetStandingPointEmpty()
    {
        Dictionary<Transform, Transform> empty = fruitContainer.standingPoints.Where(x => x.Value == null).ToDictionary(x => x.Key, x => x.Value);
        return empty.Count <= 0 ? null : empty.ElementAt(Random.Range(0, empty.Count)).Key;
    }

    public virtual void SetStandingPoint(Transform standingPoint, Transform character)
    {
        if (!fruitContainer.standingPoints.ContainsKey(standingPoint))
        {
            Debug.LogError("Standing point not found in " + gameObject.name + "!");
            return;
        }

        if (fruitContainer.standingPoints[standingPoint] != null && character != null)
        {
            Debug.LogError("Standing point is not empty!");
            return;
        }

        fruitContainer.standingPoints[standingPoint] = character;
    }

    private void UpdateResourcesNeededToUnlock()
    {
        txtUnlockPrice.text = "<sprite=0> " + resourcesNeededToUnlock;
    }

    public void OnUnlock()
    {
        isUnlocked = true;
        ReloadLockStatus();
        unlockGroup.localScale = Vector3.zero;
        unlockGroup.DOScale(Vector3.one, 0.5f).OnComplete(() =>
        {
            isUnlocking = false;
        });
    }

    private void ReloadLockStatus()
    {
        lockGroup.gameObject.SetActive(!isUnlocked);
        unlockGroup.gameObject.SetActive(isUnlocked);

        if (isUnlocked) NotificationService.Instance.Post(NotificationKey.RESCAN_PATHFINDER);
    }

    public void OnPlayerEnter()
    {
        if (isUnlocked)
        {
            if (fruitContainer.bottom != null) fruitContainer.bottom.DOScale(bottomScale * 1.25f, 0.15f);
            return;
        }

        if (!isUnlocked)
        {
            unlockCoroutine = StartCoroutine(IEUnlock());
            return;
        }
    }

    public void OnPlayerExit()
    {
        if (isUnlocked)
        {
            if (fruitContainer.bottom != null) fruitContainer.bottom.DOScale(bottomScale, 0.15f);
            if (unlockCoroutine != null)
            {
                isUnlocking = false;
                StopCoroutine(unlockCoroutine);
            }
            return;
        }

        if (!isUnlocked)
        {
            if (unlockCoroutine != null)
            {
                isUnlocking = false;
                StopCoroutine(unlockCoroutine);
            }
            return;
        }
    }

    public Fruit GetFruitData()
    {
        return data?.Fruit.Ref;
    }

    public IEnumerator IEUnlock()
    {
        if (isUnlocked || UserInfo.Instance.GetResource(ResourceType.Money) <= 0 || isUnlocking) yield break;

        isUnlocking = true;
        PlayerController player = GameManager.Instance.player;
        while (UserInfo.Instance.GetResource(ResourceType.Money) > 0 && !isUnlocked)
        {
            resourcesNeededToUnlock -= 1;
            UserInfo.Instance.AddResource(ResourceType.Money, -1);
            UpdateResourcesNeededToUnlock();

            if (resourcesNeededToUnlock % 2 == 0)
            {
                GameObject money = Instantiate(moneyPrefab, player.transform.position, Quaternion.identity);
                money.transform.DOMove(transform.position, 0.5f).OnComplete(() => Destroy(money));
            }

            if (resourcesNeededToUnlock <= 0)
            {
                OnUnlock();
                yield break;
            }

            yield return new WaitForSeconds(0.01f);
        }

    }

    public bool IsUnlocked()
    {
        return isUnlocked;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Character character))
        {
            characterNearby.Add(character);
        }

        if (other.gameObject.CompareTag("Player"))
        {
            OnPlayerEnter();
        }
    }

    public virtual void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Character character))
        {
            characterNearby.Remove(character);
        }

        if (other.gameObject.CompareTag("Player"))
        {
            OnPlayerExit();
        }
    }
}
