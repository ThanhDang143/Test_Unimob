using System.Linq;
using DG.Tweening;
using UnityEngine;

public class Tree : LockableObj
{
    [Space(10)]
    [SerializeField] private float delaySpawn = 0.5f;

    private bool isHarvesting;
    private GameObject fruitPrefab;

    public override void Init()
    {
        base.Init();
        isHarvesting = false;
        LoadFruitPrefab();
    }

    private void LoadFruitPrefab()
    {
        DataManager.Fruit fruit = data.Fruit.Ref;
        GameManager.Instance.LoadAssetAsync<GameObject>(fruit.PrefabAddress, (result) =>
        {
            fruitPrefab = result;
        });
    }

    private void Update()
    {
        if (!isUnlocked || fruitContainer == null || isUnlocking) return;

        SpawnFruits();
        PlayerGetFruit();
    }

    private void PlayerGetFruit()
    {
        if (isHarvesting) return;

        PlayerController player = GameManager.Instance.player;
        bool hasPlayerNearby = characterNearby.Contains(player);
        bool hasFruit = fruits.Count > 0;
        bool playerCanCarry = player.CanCarry();

        if (characterNearby.Contains(GameManager.Instance.player) && fruits.Count > 0 && !isHarvesting && player.CanCarry())
        {
            isHarvesting = true;
            FruitView firstFruit = fruits.FirstOrDefault();
            firstFruit.transform.DOComplete();
            firstFruit.transform.SetParent(null);
            firstFruit.transform.DOMove(player.GetFruitPos(fruits[0]), 0.25f).OnComplete(() =>
            {
                player.fruits.Add(firstFruit);
                isHarvesting = false;
            });
            firstFruit.transform.SetParent(player.fruitContainer.transform);
            fruits.RemoveAt(0);
        }
    }

    private void SpawnFruits()
    {
        if (fruits.Count < fruitContainer.fruitPoss.Count)
        {
            delaySpawn -= Time.deltaTime;
            if (delaySpawn <= 0)
            {
                delaySpawn = 0.5f;
                SpawnFruit();
            }
        }
    }

    private void SpawnFruit()
    {
        Transform fruitParent = fruitContainer.fruitPoss.FirstOrDefault(f => f.childCount == 0);
        GameObject fruit = Instantiate(fruitPrefab, fruitParent);
        fruit.transform.localScale = Vector3.zero;
        fruit.transform.DOScale(Vector3.one, 0.5f);

        fruits.Add(fruit.GetComponent<FruitView>());
    }
}
