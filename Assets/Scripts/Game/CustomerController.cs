using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Animancer.FSM;
using DataManager;
using DG.Tweening;
using Pathfinding;
using Sirenix.OdinInspector;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class CustomerController : Character
{
    [Space(10)]
    [SerializeField] private AILerpCustom aiLerp;
    [SerializeField] private AIDestinationSetter destinationSetter;

    [Space(10)]
    [SerializeField] private Image imgRequiredFruit;
    [SerializeField] private TextMeshProUGUI txtRequiredFruit;

    private CustomerState state;
    private LockableObj currentBooth;
    private int currentFruitCount;
    private int requireFruit;
    private Transform curStandingPoint;
    private Transform box;

    [PropertySpace(SpaceBefore = 10)]
    [Button]
    private void Scan()
    {
        aiLerp = GetComponent<AILerpCustom>();
    }

    public override void Init()
    {
        base.Init();
        currentBooth = ChooseBooth();
        if (box != null) Destroy(box.gameObject);
        if (currentBooth == null)
        {
            SetState(CustomerState.Inactive);
            return;
        }

        SetState(CustomerState.Moving);
        SetTarget();
        SetRequiredFruits();
        aiLerp.OnTargetReachedAction = OnTargetReached;
    }


    private LockableObj ChooseBooth(LockableObj forceBooth = null)
    {
        if (currentBooth != null)
        {
            currentBooth.SetStandingPoint(curStandingPoint, null);
        }

        if (forceBooth != null)
        {
            return forceBooth;
        }

        List<Booth> booths = GameManager.Instance.curMapController.GetActiveBooth().Where(b => b.GetStandingPointEmpty() != null).ToList();
        if (booths.Count <= 0)
        {
            SetState(CustomerState.Inactive);
            return null;
        }

        return booths[Random.Range(0, booths.Count)];
    }

    public void SetTarget(Transform forceTarget = null)
    {
        if (forceTarget == null)
        {
            curStandingPoint = currentBooth.GetStandingPointEmpty();

            currentBooth.SetStandingPoint(curStandingPoint, transform);
            destinationSetter.target = curStandingPoint;
        }
        else
        {
            curStandingPoint = forceTarget;
            destinationSetter.target = forceTarget;
        }

        DOVirtual.DelayedCall(0.25f, aiLerp.SearchPath);
    }

    private void SetRequiredFruits()
    {
        txtRequiredFruit.transform.parent.gameObject.SetActive(true);
        currentFruitCount = 0;
        Fruit fruit = currentBooth.GetFruitData();
        imgRequiredFruit.sprite = fruit.Icon.Get<Sprite>();
        requireFruit = Random.Range(2, 5);
        UpdateRequireCounter();
    }

    public void UpdateRequireCounter()
    {
        txtRequiredFruit.text = currentFruitCount + "/" + requireFruit;
    }

    public bool CanGetFruit()
    {
        return IsState(CustomerState.Waiting) && currentFruitCount < requireFruit;
    }

    public void SetState(CustomerState _state)
    {
        state = _state;
    }

    public CustomerState GetState()
    {
        return state;
    }

    public bool IsState(CustomerState _state)
    {
        return state == _state;
    }
    public void OnGetFruit()
    {
        currentFruitCount++;
        UpdateRequireCounter();

        if (!CanGetFruit())
        {
            txtRequiredFruit.transform.parent.gameObject.SetActive(false);
            currentBooth = ChooseBooth(GameManager.Instance.curMapController.checkOutTable);
            SetState(CustomerState.Moving);
            SetTarget();
        }
    }

    public void CheckOutComplete(Transform _box)
    {
        box = _box;
        MapController mapController = GameManager.Instance.curMapController;
        currentBooth.SetStandingPoint(curStandingPoint, null);
        SetState(CustomerState.Moving);
        SetTarget(mapController.GetSpawnPoint());

    }

    public void ReorderStandingPoint()
    {
        CheckOutTable checkOutTable = GameManager.Instance.curMapController.checkOutTable;

        Transform newStandingPoint = checkOutTable.GetCheckoutPointByValue(transform);
        SetState(CustomerState.Moving);
        SetTarget(newStandingPoint);
    }

    private void OnTargetReached()
    {
        SetState(CustomerState.Waiting);
        if (currentBooth != null) transform.DOLookAt(currentBooth.transform.position, 0.25f);

        // Gone Home
        if (box != null)
        {
            SetState(CustomerState.Inactive);
        }
    }

    public override void MovePlayer()
    {
        base.MovePlayer();

        if (IsState(CustomerState.Moving) || aiLerp.velocity.magnitude > 0)
        {
            aiLerp.MovementUpdate(Time.deltaTime, out Vector3 nextPosition, out Quaternion nextRotation);
            aiLerp.FinalizeMovement(nextPosition, nextRotation);
        }

        PlayAnimation(aiLerp.velocity.magnitude);
    }
}

public enum CustomerState
{
    Moving,
    Waiting,
    Inactive
}
