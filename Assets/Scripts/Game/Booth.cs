using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;
using Unity.VisualScripting;
using DataManager;

public class Booth : LockableObj
{
    [Space(10)]
    [SerializeField] private Image icon;
    private bool isBoothGettingFruit;
    private bool isCustomerGettingFruit;
    private CustomerController customerGettingFruit;

    public override void Init()
    {
        base.Init();
        LoadIcon();
    }

    private void LoadIcon()
    {
        icon.sprite = data.Icon.Get<Sprite>();
    }

    private void Update()
    {
        if (!isUnlocked || fruitContainer == null || isUnlocking) return;

        BoothGetFruit();
        CustomerGetFruit();
    }

    private void CustomerGetFruit()
    {
        if (isCustomerGettingFruit) return;

        bool hasFruit = fruits.Count > 0;
        bool hasCustomerNearby = characterNearby.Count(c => c is CustomerController controller && controller.CanGetFruit()) > 0;

        if (hasFruit && hasCustomerNearby)
        {
            isCustomerGettingFruit = true;
            customerGettingFruit = customerGettingFruit != null ? customerGettingFruit : (CustomerController)characterNearby.FirstOrDefault(c => c is CustomerController controller && controller.CanGetFruit());
            FruitView lastFruit = fruits.LastOrDefault();

            fruits.Remove(lastFruit);
            customerGettingFruit.fruits.Add(lastFruit);
            lastFruit.transform.SetParent(null);
            lastFruit.transform.DOMove(customerGettingFruit.GetFruitPos(lastFruit), 0.25f).OnComplete(() =>
            {
                lastFruit.transform.SetParent(customerGettingFruit.fruitContainer.transform);
                lastFruit.transform.localScale = Vector3.one;
                lastFruit.transform.localPosition = Vector3.zero;
                customerGettingFruit.OnGetFruit();
                isCustomerGettingFruit = false;
                if (!customerGettingFruit.CanGetFruit())
                {
                    customerGettingFruit = null;
                }
            });
        }
    }

    private void BoothGetFruit()
    {
        if (isBoothGettingFruit) return;

        PlayerController player = GameManager.Instance.player;
        bool hasSlot = fruits.Count < fruitContainer.fruitPoss.Count;
        bool hasPlayerNearby = characterNearby.Contains(player);
        bool playerHasFruit = player.IsCarrying();

        if (hasSlot && hasPlayerNearby && playerHasFruit)
        {
            for (int i = player.fruits.Count - 1; i >= 0; i--)
            {
                FruitView fruit = player.fruits[i];
                if (fruit.GetID() == data.Fruit.Ref.Id)
                {
                    isBoothGettingFruit = true;
                    Transform slot = fruitContainer.fruitPoss.FirstOrDefault(f => f.childCount == 0);
                    fruit.transform.DOComplete();
                    fruit.transform.SetParent(null);
                    fruit.transform.DOMove(slot.position, 0.25f).OnComplete(() =>
                    {
                        fruit.transform.SetParent(slot);
                        fruits.Add(fruit);
                        player.fruits.Remove(fruit);
                        isBoothGettingFruit = false;
                    });
                    break;
                }
            }
        }
    }
}
