using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine;

public class CheckOutTable : LockableObj
{
    [Space(10)]
    [SerializeField] private Transform boxTransform;
    [SerializeField] private Transform moneyTransform;

    private GameObject boxPrefab;
    private bool isCheckingOut;
    private bool isGetingMoney;
    private Box box;
    private List<Money> allMoney = new List<Money>();
    private float debugCouneter;

    public override void Init()
    {
        isUnlocked = true;
        debugCouneter = 0;
        base.Init();

        allMoney = new List<Money>();
        GameManager.Instance.LoadAssetAsync<GameObject>(PrefabsAddress.Box, (result) => { boxPrefab = result; });
        GameManager.Instance.LoadAssetAsync<GameObject>(PrefabsAddress.Money, (result) => { moneyPrefab = result; });
    }

    public override Transform GetStandingPointEmpty()
    {
        for (int i = 0; i < fruitContainer.standingPoints.Count; i++)
        {
            KeyValuePair<Transform, Transform> standingPoint = fruitContainer.standingPoints.ElementAt(i);
            if (standingPoint.Value == null) return standingPoint.Key;
        }

        return null;
    }

    public void StartCheckout()
    {
        StartCoroutine(IECheckOut());
    }

    private IEnumerator IECheckOut()
    {
        CustomerController customerCheckingOut = fruitContainer.standingPoints.ElementAt(0).Value.GetComponent<CustomerController>();
        if (box == null)
        {
            GameObject boxObj = Instantiate(boxPrefab, GameManager.Instance.curMapController.transform);
            box = boxObj.GetComponent<Box>();
            boxObj.transform.position = boxTransform.position;
            boxObj.transform.localScale = Vector3.zero;
            boxObj.transform.DOScale(Vector3.one, 0.25f);

            yield return new WaitForSeconds(0.5f);
        }

        int moneyValue = 0;
        for (int i = customerCheckingOut.fruits.Count - 1; i >= 0; i--)
        {
            FruitView fruit = customerCheckingOut.fruits[i];
            moneyValue += fruit.GetSellPrice();
            Transform emptyFruitPos = box.GetEmptyFruitPos();
            fruit.transform.SetParent(null);
            customerCheckingOut.fruits.RemoveAt(i);
            fruit.transform.DOKill();
            fruit.transform.DOJump(emptyFruitPos.position, 2f, 1, 0.5f).OnComplete(() =>
            {
                fruit.transform.SetParent(emptyFruitPos);
            });
            yield return new WaitForSeconds(0.5f);
        }

        box.Close();

        yield return new WaitForSeconds(1.5f);

        GameObject moneyObj = Instantiate(moneyPrefab);
        moneyObj.transform.position = customerCheckingOut.transform.position;
        Money money = moneyObj.GetComponent<Money>();
        money.Value = moneyValue;
        moneyObj.transform.DOJump(moneyTransform.position + Vector3.up * 0.1f * allMoney.Count, 1.5f, 1, 0.5f).OnComplete(() =>
        {
            allMoney.Add(money);
        });

        box.transform.DOJump(customerCheckingOut.transform.position + Vector3.up * 2.75f, 1f, 1, 0.5f).OnComplete(() =>
        {
            box.transform.SetParent(customerCheckingOut.transform);
            customerCheckingOut.CheckOutComplete(box.transform);
            isCheckingOut = false;
            box = null;
            ReorderCustomer();
        });
    }

    public Transform GetCheckoutPointByValue(Transform customer)
    {
        return fruitContainer.standingPoints.FirstOrDefault(p => p.Value == customer).Key; ;
    }

    private void Update()
    {
        PlayerGetMoney();
        Checkout();

        Debug();
    }

    private void Debug()
    {
        if (fruitContainer.standingPoints.Count(x => x.Value != null) > 0 && fruitContainer.standingPoints.ElementAt(0).Value == null)
        {
            debugCouneter += Time.deltaTime;
            if (debugCouneter >= 1f)
            {
                ReorderCustomer();
                debugCouneter = 0;
            }
        }
    }

    public void ReorderCustomer()
    {
        for (int i = 0; i < fruitContainer.standingPoints.Count - 1; i++)
        {
            KeyValuePair<Transform, Transform> standingPoint = fruitContainer.standingPoints.ElementAt(i);
            if (standingPoint.Value == null)
            {
                for (int j = i + 1; j < fruitContainer.standingPoints.Count; j++)
                {
                    KeyValuePair<Transform, Transform> nextStandingPoint = fruitContainer.standingPoints.ElementAt(j);
                    if (nextStandingPoint.Value != null)
                    {
                        (fruitContainer.standingPoints[standingPoint.Key], fruitContainer.standingPoints[nextStandingPoint.Key]) = (fruitContainer.standingPoints[nextStandingPoint.Key], fruitContainer.standingPoints[standingPoint.Key]);
                        break;
                    }
                }
            }
        }

        foreach (KeyValuePair<Transform, Transform> standingPoint in fruitContainer.standingPoints)
        {
            if (standingPoint.Value == null) continue;

            standingPoint.Value.GetComponent<CustomerController>().ReorderStandingPoint();
        }
    }

    private void Checkout()
    {
        if (isCheckingOut || !CanCheckout()) return;

        isCheckingOut = true;
        StartCheckout();
    }

    public void PlayerGetMoney()
    {
        if (isGetingMoney || !characterNearby.Contains(GameManager.Instance.player)) return;

        for (int i = allMoney.Count - 1; i >= 0; i--)
        {
            Money money = allMoney[i];
            money.transform.SetParent(null);
            isGetingMoney = true;
            money.transform.DOJump(GameManager.Instance.player.transform.position, 2f, 1, 0.5f).OnComplete(() =>
            {
                isGetingMoney = false;
                Destroy(money.gameObject);
                UserInfo.Instance.AddResource(ResourceType.Money, money.Value);
            });
            allMoney.RemoveAt(i);
        }
    }

    private bool CanCheckout()
    {
        bool hasPlayer = characterNearby.Contains(GameManager.Instance.player);
        bool hasCustomer = fruitContainer.standingPoints.ElementAt(0).Value != null;

        if (!hasPlayer || !hasCustomer) return false;

        CustomerController customer = fruitContainer.standingPoints.ElementAt(0).Value.GetComponent<CustomerController>();

        bool customerIsWaiting = customer.IsState(CustomerState.Waiting);
        bool customerIsOnCheckoutPoint = Vector3.Distance(customer.transform.position, fruitContainer.standingPoints.ElementAt(0).Key.position) < 0.6f;

        return customerIsWaiting && customerIsOnCheckoutPoint;
    }
}
