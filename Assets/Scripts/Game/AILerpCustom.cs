using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class AILerpCustom : AILerp
{
    public Action OnTargetReachedAction;

    public override void OnTargetReached()
    {
        base.OnTargetReached();
        OnTargetReachedAction?.Invoke();
    }
}
