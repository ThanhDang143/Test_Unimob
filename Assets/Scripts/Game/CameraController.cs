using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraController : MonoSingleton<CameraController>
{
    [Space(10)]
    [SerializeField] private List<CinemachineVirtualCamera> vCams;

    public CinemachineVirtualCamera GetVirtualCamera(int index = 0)
    {
        vCams ??= new List<CinemachineVirtualCamera>();
        if (index < 0 || index >= vCams.Count)
        {
            Debug.LogError("Index out of range!");
            return null;
        }

        return vCams[index];
    }
}
