using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class FruitContainerView : SerializedMonoBehaviour
{
    public Transform bottom;
    public List<Transform> fruitPoss;

    /// <summary>
    /// Key: standingPoint transform
    /// Value: character in standingPoint
    /// </summary>
    public Dictionary<Transform, Transform> standingPoints = new Dictionary<Transform, Transform>();

    [Button]
    private void Scan()
    {
        fruitPoss.Clear();
        standingPoints.Clear();

        fruitPoss = transform.GetComponentsInChildren<Transform>().Where(t => t.name.Contains("P (")).ToList();

        foreach (Transform t in transform.GetComponentsInChildren<Transform>().Where(t => t.name.Contains("StandingPoint (")).ToList())
        {
            standingPoints.Add(t, null);
        }
    }
}
