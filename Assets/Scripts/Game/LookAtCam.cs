using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    void Update()
    {
        if (CameraController.Instance == null) return;
        transform.LookAt(CameraController.Instance.transform);
    }
}
