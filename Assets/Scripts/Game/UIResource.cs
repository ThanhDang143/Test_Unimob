using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIResource : MonoBehaviour
{
    [SerializeField] private ResourceType resourceType;
    [SerializeField] private TextMeshProUGUI txtResource;

    private void OnEnable()
    {
        Reload();
        NotificationService.Instance.Subscribe(NotificationKey.ON_RESOURCE_UPDATE, Reload);
    }

    private void OnDisable()
    {
        NotificationService.Instance.Unsubscribe(NotificationKey.ON_RESOURCE_UPDATE, Reload);
    }

    private void Reload()
    {
        txtResource.text = UserInfo.Instance.GetResource(resourceType).ToString();
    }
}
