using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfo : Singleton<UserInfo>
{
    /// <summary>
    /// Count from 0.
    /// </summary>
    public int currentMap = 0;

    /// <summary>
    /// User's current resources.
    /// </summary>
    private Dictionary<ResourceType, int> resources = new()
    {
        { ResourceType.Money, 0 },
    };

    public int GetResource(ResourceType resourceType)
    {
        resources.TryAdd(resourceType, 0);
        return resources[resourceType];
    }

    public void SetResource(ResourceType resourceType, int value)
    {
        if (!resources.TryAdd(resourceType, value))
        {
            resources[resourceType] = value;
        }

        GameManager.Instance.CheckWin();
        NotificationService.Instance.Post(NotificationKey.ON_RESOURCE_UPDATE);
    }

    public void AddResource(ResourceType resourceType, int value)
    {
        int resourcesValue = GetResource(resourceType) + value;
        SetResource(resourceType, resourcesValue);
    }

}
