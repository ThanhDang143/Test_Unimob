using Animancer;
using Cinemachine;
using UnityEngine;

public class PlayerController : Character
{
    [Space(10)]
    [SerializeField] protected Transform playerView;

    public override void Init()
    {
        base.Init();
        GameManager.Instance.player = this;
        playerView = playerView != null ? playerView : transform.Find("View");
        SetupCamera();
    }

    private void SetupCamera()
    {
        if (CameraController.Instance == null)
        {
            Debug.LogError("CameraController not found!");
            return;
        }

        CinemachineVirtualCamera vCam = CameraController.Instance.GetVirtualCamera();

        if (vCam == null)
        {
            Debug.LogError("VirtualCamera not found!");
            return;
        }

        vCam.Follow = transform;
        vCam.LookAt = transform;
    }

    public override void MovePlayer()
    {
        base.MovePlayer();

        Vector2 input = ScreenGame.Instance.GetJoystickInput();

        Vector3 move = transform.right * input.x + transform.forward * input.y;
        if (transform.position.y > 0) move.y = -moveSpeed;

        characterController.Move(moveSpeed * Time.deltaTime * move);

        playerView.LookAt(transform.position + new Vector3(move.normalized.x, 0, move.normalized.z));

        PlayAnimation(input.magnitude);
    }
}
