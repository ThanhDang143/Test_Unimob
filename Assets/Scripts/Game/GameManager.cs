using System.Collections;
using System.Collections.Generic;
using DataManager;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameManager : Singleton<GameManager>
{
    public PlayerController player;
    public MapController curMapController;
    public GameObject playerPrefab;
    public GameObject customerPrefab;

    public void Init()
    {
        LoadAssetAsync<GameObject>(PrefabsAddress.Player, result => playerPrefab = result);
        LoadAssetAsync<GameObject>(PrefabsAddress.Customer, result => customerPrefab = result);
    }

    public void LoadMap(int index)
    {
        Map mapData = DataService.Instance.GetMapByIndex(UserInfo.Instance.currentMap);
        InstantiateAsync(mapData.PrefabAddress, null, result =>
        {
            curMapController = result.GetComponent<MapController>();
            curMapController.Init(mapData);
        });
    }

    public void LoadGame()
    {
        if (curMapController != null)
        {
            curMapController.Destroy();
        }
        LoadMap(UserInfo.Instance.currentMap);
    }

    public void NextMap()
    {
        UserInfo.Instance.currentMap++;
        UserInfo.Instance.SetResource(ResourceType.Money, 0);
        LoadGame();
    }

    public void CheckWin()
    {
        if (UserInfo.Instance.GetResource(ResourceType.Money) >= curMapController.GetWinCondition() && !SSSceneManager.Instance.IsShowingPopUp())
        {
            SSSceneManager.Instance.PopUp(PopupNames.WIN);
        }
    }

    public T LoadAssetAsync<T>(string address, System.Action<T> onComplete = null) where T : Object
    {
        if (string.IsNullOrEmpty(address) || string.IsNullOrWhiteSpace(address))
        {
            Debug.Log("<color=red>Address can not null!!!</color>");
            return null;
        }

        AsyncOperationHandle<T> handle = Addressables.LoadAssetAsync<T>(address);
        handle.Completed += (result) =>
        {
            onComplete?.Invoke(result.Result);
            Addressables.Release(handle);
        };

        return handle.Result;
    }

    public GameObject InstantiateAsync(string address, Transform parent, System.Action<GameObject> onComplete = null)
    {
        if (string.IsNullOrEmpty(address) || string.IsNullOrWhiteSpace(address))
        {
            Debug.Log("<color=red>Address can not null!!!</color>");
            return null;
        }

        AsyncOperationHandle<GameObject> handle = Addressables.InstantiateAsync(address, parent);
        handle.Completed += (result) =>
        {
            onComplete?.Invoke(result.Result);
            // Addressables.Release(handle);
        };

        return handle.Result;
    }

}
