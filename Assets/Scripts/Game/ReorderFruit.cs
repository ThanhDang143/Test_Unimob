using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ReorderFruit : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(IEReorder());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator IEReorder()
    {
        while (true)
        {
            FruitView[] fruitViews = transform.GetComponentsInChildren<FruitView>();
            for (int i = 0; i < fruitViews.Length; i++)
            {
                PlayerController player = GameManager.Instance.player;
                Vector3 targetPos = i * fruitViews[i].GetSize().y * Vector3.up;
                if (fruitViews[i].transform.position != targetPos)
                {
                    fruitViews[i].transform.DOLocalMove(targetPos, 0.25f);
                }
            }

            yield return new WaitForSeconds(0.2f);
        }
    }
}
