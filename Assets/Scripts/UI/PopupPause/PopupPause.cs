

public class PopupPause : SSController
{
    public void OnBtnHomeClicked()
    {
        SSSceneManager.Instance.Close();
        UserInfo.Instance.SetResource(ResourceType.Money, 0);
        SSSceneManager.Instance.Screen(ScreenNames.HOME);
    }
}
