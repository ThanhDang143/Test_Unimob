using System.Collections;
using System.Collections.Generic;
using DataManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScreenGame : SSSingleton<ScreenGame>
{
    [Space(10)]
    [SerializeField] private FloatingJoystick joystick;
    [SerializeField] private TextMeshProUGUI txtRequiredMoney;

    public override void Awake()
    {
        base.Awake();
        joystick = joystick != null ? joystick : FindObjectOfType<FloatingJoystick>();
    }

    public void OnBtnPauseClicked()
    {
        SSSceneManager.Instance.PopUp(PopupNames.PAUSE);
    }

    public Vector2 GetJoystickInput()
    {
        if (joystick == null)
        {
            Debug.LogError("Joystick not found!");
            return Vector2.zero;
        }

        float horizontal = joystick.Horizontal;
        float vertical = joystick.Vertical;

        return new Vector2(horizontal, vertical);
    }

    public void SetRequiredMoney(int money)
    {
        txtRequiredMoney.text = money.ToString();
    }
}
