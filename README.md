# Test_Unimob

[Download Apk](https://gitlab.com/ThanhDang143/Test_Unimob/-/raw/main/Release/ThanhDV.apk?inline=false)

### Game
- Mở game -> Play
- Next map khi đạt đủ số tiền yêu cầu (màu đỏ)
- Khi qua map mới reset lại tiền
- Hiện tại có 2 map
- Về home reset lại màn chơi (không reset về map 1)
- Chưa có save game

### Unity
- Chạy project từ SceneManager
- Thêm map
    - Clone map base
    - Đổi tên, đổi addressable
    - Thêm các khu vực bằng cách kéo prefab lên map (booth, tree)
    -  Chọn ID (ObjID) và chọn trạng thái ban đầu (IsUnlocked)
    - Trở lại mapPrefab chọn Scan để load reference
    - Scale pathfinder sao cho bao phủ hết các object trên map
    - Update data *trong Assets/Database/RawData/Map.xlsx* để thêm map
    - Thứ tự trong excel cũng là thứ tự các map trong game
- Cập nhật data
    - Tương tác với data thông qua file excel tại *Assets/Database/RawData*
    - Sau khi thay cập nhật data trong excel *Tools > Data Manager > Update data* để load lại data đã chỉnh sửa vào game

